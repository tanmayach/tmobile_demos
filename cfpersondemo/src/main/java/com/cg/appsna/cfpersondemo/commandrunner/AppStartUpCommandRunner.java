package com.cg.appsna.cfpersondemo.commandrunner;

//import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.cg.appsna.cfpersondemo.entity.Person;
import com.cg.appsna.cfpersondemo.service.IPersonService;

@Component
public class AppStartUpCommandRunner implements CommandLineRunner {

	@Autowired
	IPersonService service;
	
	@Override
	public void run(String... args) throws Exception {
		Person person1 = new Person();
		
		person1.setId(101);
		person1.setpName("Uma");
		person1.setpAddress("Bangalore");
		
		Person person2 = new Person();
		person2.setId(102);
		person2.setpName("Tanmay");
		person2.setpAddress("Bangalore");
		
		Person person3 = new Person();
		person3.setId(103);
		person3.setpName("Rahul");
		person3.setpAddress("Pune");
		
		service.save(person1);
		service.save(person2);
		service.save(person3);
		
		/*
		 * List<Person> personss = service. for (Person customer : customers) {
		 * System.out.println(customer.getName()); }
		 */
		
		//service.getAllPersons().stream().map((person)->person.getpName()).forEach((nm)->System.out.println(nm));
		
		
	}

}
