package com.cg.appsna.cfpersondemo.controller;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cg.appsna.cfpersondemo.entity.Person;
//import com.cg.appsna.cfpersondemo.exception.PersonException;
import com.cg.appsna.cfpersondemo.exception.PersonIDInvalidException;
import com.cg.appsna.cfpersondemo.exception.PersonNotFoundException;
import com.cg.appsna.cfpersondemo.service.IPersonService;

@RestController
public class PersonController {

	Logger logger = LoggerFactory.getLogger(PersonController.class);
	
	@Autowired
	IPersonService service;
	
	@GetMapping("/persons")
	public List<Person> getAll(){
		
		logger.info("getAll () is started "); 
		List<Person> pList = service.getAllPersons();
		
		if(pList.isEmpty() || pList==null) {
			logger.warn("Sorry No Persons available to show !");
		}
		logger.debug(" Person data fetched !");
		return service.getAllPersons();
	}
	
	@PostMapping("/addPerson")
	public void addPerson(@RequestBody Person person) {
		
		service.save(person);
	}
	
	@GetMapping("/person/{id}")
	public Person getPerson(@PathVariable("id") String id) {
		
		System.out.println("hyyyy");
		logger.info("Enter to getPerson! "+id);
		
		if(id==null)
			throw new PersonNotFoundException("Please Enter the Id to see the details!");
		if(!isValid(id)) {
			
			logger.error("Person Id "+ id+" required and should be in Number format!");
			throw new PersonIDInvalidException("Id required and should be in Number format!");
			
		}
		
		Person per = service.getPerson(Integer.parseInt(id));
		
		if(per==null) {
			
			logger.error("Person Id "+ id+" is notr available in Database!");
			throw new PersonNotFoundException("For the "+id+" given , No person data available.Sorry !");
			
		}
		
			return per;
		
		
	}
	
	public boolean isValid(String id) {
		
		boolean flag = false;
		Pattern pat = Pattern.compile("^\\d+$");
		Matcher mat = pat.matcher(id);
		if(mat.find())
			flag=true;;
		
			System.out.println("flag ="+flag);
		return flag;
	}
	
}
