package com.cg.appsna.cfpersondemo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cg.appsna.cfpersondemo.entity.Person;


public interface PersonRepo extends JpaRepository<Person, Integer> {

}
