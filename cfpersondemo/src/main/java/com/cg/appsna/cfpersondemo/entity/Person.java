package com.cg.appsna.cfpersondemo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.stereotype.Component;

@Entity
public class Person {

	@Id
	@GeneratedValue
	private int id;
	
	private String pName;
	
	private String pAddress;


	 public Person() {
		 
	 }
	 

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Person(int id, String pName, String pAddress) {
		super();
		this.id = id;
		this.pName = pName;
		this.pAddress = pAddress;
	}

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public String getpAddress() {
		return pAddress;
	}

	public void setpAddress(String pAddress) {
		this.pAddress = pAddress;
	}

}
