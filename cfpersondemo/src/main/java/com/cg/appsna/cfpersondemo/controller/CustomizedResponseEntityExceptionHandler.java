package com.cg.appsna.cfpersondemo.controller;

import java.util.Date;

//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
//import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.cg.appsna.cfpersondemo.exception.ErrorDetails;
import com.cg.appsna.cfpersondemo.exception.PersonIDInvalidException;
import com.cg.appsna.cfpersondemo.exception.PersonNotFoundException;


@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(PersonNotFoundException.class)
	//@ExceptionHandler(Exception.class)
	  public final ResponseEntity<ErrorDetails> handleUserNotFoundException(PersonNotFoundException ex, WebRequest request) {
	    ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(),
	        request.getDescription(false));
	    return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	  }
	
	@ExceptionHandler(PersonIDInvalidException.class)
	public final ResponseEntity<ErrorDetails> handleIdInvalidExceptions(PersonIDInvalidException ex, WebRequest request) {
	  ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(),
	      request.getDescription(false));
	  return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ErrorDetails> handleAllExceptions(Exception ex, WebRequest request) {
	  ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(),
	      request.getDescription(false));
	  return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}
	
	/*
	 * @ExceptionHandler(value={NoHandlerFoundException.class})
	 * 
	 * @ResponseStatus(code=HttpStatus.BAD_REQUEST) public ErrorDetails
	 * badRequest(Exception ex, HttpServletRequest request, HttpServletResponse
	 * response) { ex.printStackTrace(); return new ErrorDetails(new Date(),
	 * ex.getMessage(), ((WebRequest) request).getDescription(false)); //(400,
	 * HttpStatus.BAD_REQUEST.getReasonPhrase()); }
	 */
	
}
