package com.cg.appsna.cfpersondemo.service;

import java.util.List;

import com.cg.appsna.cfpersondemo.entity.Person;

public interface IPersonService {
	public List<Person> getAllPersons();
	public void save(Person pers);
	public Person getPerson(int id);
	
}
