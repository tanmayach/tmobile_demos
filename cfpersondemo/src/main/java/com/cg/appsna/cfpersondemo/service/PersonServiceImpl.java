package com.cg.appsna.cfpersondemo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.appsna.cfpersondemo.entity.Person;
import com.cg.appsna.cfpersondemo.repositories.PersonRepo;

@Service
@Transactional
public class PersonServiceImpl implements IPersonService {

	@Autowired
	PersonRepo repo;
	@Override
	public List<Person> getAllPersons() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}
	@Override
	public void save(Person pers) {
		// TODO Auto-generated method stub
		//System.out.println("hello add service");
		repo.save(pers);
	}
	@Override
	public Person getPerson(int id) {
		// TODO Auto-generated method stub
		//int pid= (int)id;
		return repo.findOne(id);
	}

	
}
