package com.cg.appsna.cfpersondemo.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class PersonIDInvalidException extends RuntimeException {

	
	public PersonIDInvalidException(String message) {
		super(message);
	}
}
