package com.cg.appsna.cfpersondemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.DispatcherServlet;
//import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

//import com.cg.appsna.cfpersondemo.controller.PersonController;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
//@EnableWebMvc
//@ComponentScan(basePackageClasses=PersonController.class)
public class CfpersondemoApplication {

	/*
	 * @Autowired private DispatcherServlet servlet;
	 */
	public static void main(String[] args) {
		SpringApplication.run(CfpersondemoApplication.class, args);
	}
	
	/*
	 * @Bean public CommandLineRunner getCommandLineRunner(ApplicationContext
	 * context) { servlet.setThrowExceptionIfNoHandlerFound(true); return args ->
	 * {}; }
	 */
}

